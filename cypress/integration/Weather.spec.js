it("Tests the search functionality", () => {
  cy.server();
  cy.route(`${process.env.REACT_APP_OWM_URL}*`, "fixture:cities.json").as(
    "getCities"
  );
  cy.visit("/");
  cy.get("[data-cypress=citySearch]").type("Vancouver");
  cy.get("[data-cypress=city]").should("have.length", 2);
  cy.get("[data-cypress=city]")
    .first()
    .click();
  cy.get("[data-cypress=dayCell]").should("have.length", 6);
});

it("Tests that an error is displayed if no cities are found for the search term", () => {
  cy.server();
  cy.route(`${process.env.REACT_APP_OWM_URL}*`, []).as("getCities");
  cy.visit("/");
  cy.get("[data-cypress=citySearch]").type("Land of Nod");
  cy.get("[data-cypress=failState]").should(
    "contain",
    "There are no cities for that search term"
  );
});

// Failing test! There is no error handling if the city server is down so we expect this to fail
it("Tests that an error is shown if the city search server is unavailable", () => {
  cy.server();
  cy.route({
    method: "GET",
    url: `${process.env.REACT_APP_OWM_URL}*`,
    status: 503,
    response: {},
  })
    // alias this route so we can wait on it later
    .as("getCities");
  cy.visit("/");
  cy.get("[data-cypress=citySearch]").type("Land of Nod");
  cy.get("[data-cypress=failState]").should("contain", "Some error");
});
