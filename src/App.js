import React, { Component } from "react";
import "./App.css";
import WeatherContainer from "./components/WeatherContainer";

class App extends Component {
  render() {
    return <WeatherContainer />;
  }
}

export default App;
