import moment from "moment";
import React from "react";
import styled from "styled-components";
import WeatherCell from "./WeatherCell";

const CellWrapper = styled.div`
  width: 90%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;

const DayCell = styled.div`
  width: 20%;
  margin-bottom: 10px;
  flex: 1;
`;

const CellHeader = styled.span`
  font-size: 1.5em;
  font-weight: bold;
`;

const WeatherMap = ({ weather, city }) => {
  return (
    <CellWrapper>
      {Object.keys(weather).map(key => (
        <DayCell data-cypress="dayCell" key={key}>
          <CellHeader key={key}>
            {moment(weather[key][0].dt_txt).format("dddd")}
          </CellHeader>
          <WeatherCell day={weather[key]} key={weather[key].dt} />
        </DayCell>
      ))}
    </CellWrapper>
  );
};

WeatherMap.propTypes = {};

export default WeatherMap;
