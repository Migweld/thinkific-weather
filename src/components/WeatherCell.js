import moment from "moment";
import React from "react";
import styled from "styled-components";

const PeriodWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  border: solid 1px #000;
  margin: 0 5px;
`;

const PeriodCell = styled.div`
  width: 100%;
  text-align: center;
  border-bottom: solid 1px #808080;
`;

const PeriodItem = styled.div`
  flex: 1;
  margin: 5px 0 10px 0;
`;

const WeatherCell = ({ day }) => (
  <PeriodWrapper>
    {day.map(period => (
      <PeriodCell key={period.dt}>
        <PeriodItem>{moment(period.dt_txt).format("HH:mm")}</PeriodItem>
        <PeriodItem>{period.weather[0].main}</PeriodItem>
        <PeriodItem>
          Temperature: {Math.floor(period.main.temp)}&deg;C
        </PeriodItem>
        <PeriodItem>
          Max Temperature: {Math.floor(period.main.temp_max)}&deg;C
        </PeriodItem>
        <PeriodItem>
          Min Temperature: {Math.floor(period.main.temp_min)}&deg;C
        </PeriodItem>
        <PeriodItem>Humidity: {period.main.humidity}%</PeriodItem>
      </PeriodCell>
    ))}
  </PeriodWrapper>
);

WeatherCell.propTypes = {};

export default WeatherCell;
