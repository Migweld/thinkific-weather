import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import CitiesList from "./CitiesList";

const CityInput = styled.div`
  input[type="text"] {
    max-width: none;
    margin-bottom: 0;
    padding: 10px 10px;
    font-size: 2em;
    text-align: center;
  }
`;

const AppHeader = styled.h1`
  width: 100%;
  margin: 0 auto;
  text-align: center;
  margin-bottom: 50px;
`;

const CitySearch = ({
  handleGetWeather,
  handleChange,
  cities,
  touched,
  isLoadingCity,
  isLoadingWeather,
}) => (
  <div>
    <AppHeader>Weather Forecast</AppHeader>
    <CityInput>
      <input
        className="standard-input"
        type="text"
        name="citySearch"
        onChange={handleChange}
        placeholder="Enter a city name"
        data-cypress="citySearch"
      />
    </CityInput>
    {touched ? (
      <CitiesList
        cities={cities}
        handleClick={handleGetWeather}
        isloading={isLoadingWeather}
      />
    ) : null}
  </div>
);

CitySearch.propTypes = {
  handleGetWeather: PropTypes.func.isRequired,
  cities: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      country: PropTypes.string,
      coord: PropTypes.shape({
        lon: PropTypes.number,
        lat: PropTypes.number,
      }),
    })
  ),
  touched: PropTypes.bool.isRequired,
  isLoadingCity: PropTypes.bool.isRequired,
  isLoadingWeather: PropTypes.bool.isRequired,
};

export default CitySearch;
