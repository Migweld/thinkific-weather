import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";

export const City = styled.div`
  padding: 10px 30px;
  border: 1px solid #000;
  cursor: pointer;
  &:hover {
    background: #0000ff;
    color: #fff;
  }
  &:first-child {
    margin-top: 20px;
  }
`;

const CitiesList = ({ cities, handleClick }) => (
  <div data-cypress="cityList">
    {cities === null ? (
      <p data-cypress="failState">There are no cities for that search term</p>
    ) : (
      cities.map(city => (
        <City
          key={city.id}
          data-countryid={city.id}
          onClick={handleClick}
          data-cypress="city">
          {city.name}, {city.country}
        </City>
      ))
    )}
  </div>
);

CitiesList.propTypes = {
  cities: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      country: PropTypes.string,
      coord: PropTypes.shape({
        lon: PropTypes.number,
        lat: PropTypes.number,
      }),
    })
  ),
  handleClick: PropTypes.func.isRequired,
};

export default CitiesList;
