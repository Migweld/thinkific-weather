import groupBy from "lodash.groupby";
import React, { Component } from "react";
import styled from "styled-components";
import { debounce } from "throttle-debounce";
import CitySearch from "./CitySearch";
import WeatherMap from "./WeatherMap";

const Wrapper = styled.div`
  margin-top: 30px;
  display: flex;
  justify-content: center;
`;

class WeatherContainer extends Component {
  constructor(props) {
    super(props);
    this.filterCities = debounce(700, this.filterCities);
    this.state = {
      cities: null,
      city: null,
      searchTerm: null,
      weather: null,
      isLoadingCity: false,
      isLoadingWeather: false,
      touched: false,
    };
  }

  filterCities = () => {
    const { searchTerm } = this.state;
    this.setState({ isLoading: true, touched: true });
    if (searchTerm.length > 3) {
      fetch(`${process.env.REACT_APP_OWM_URL}?search=${searchTerm}`)
        .then(response => response.json())
        .then(data => this.setState({ cities: data, isLoading: false }))
        .catch(error => console.log(error));
    }
  };

  groupByDay = data => {
    return groupBy(data.list, segment => {
      let date = new Date(segment.dt_txt);
      return Math.floor(date.getTime() / (1000 * 60 * 60 * 24));
    });
  };

  handleGetWeather = event => {
    this.setState({
      isLoadingWeather: true,
    });
    fetch(
      `https://api.openweathermap.org/data/2.5/forecast?id=${
        event.target.dataset.countryid
      }&units=metric&APPID=472a13c3e8414d31c164502b5318a22b`
    )
      .then(response => response.json())
      .then(data =>
        this.setState({
          weather: this.groupByDay(data),
          city: data.city,
          isLoadingWeather: false,
        })
      );
  };

  handleSearchChange = event => {
    this.setState(
      {
        searchTerm: event.target.value,
      },
      this.filterCities
    );
  };

  render() {
    const { weather, city } = this.state;
    return (
      <Wrapper>
        {weather !== null ? (
          <WeatherMap weather={weather} city={city} />
        ) : (
          <CitySearch
            handleGetWeather={this.handleGetWeather}
            handleChange={this.handleSearchChange}
            cities={this.state.cities}
            isLoadingCity={this.state.isLoadingCity}
            isLoadingWeather={this.state.isLoadingWeather}
            touched={this.state.touched}
          />
        )}
      </Wrapper>
    );
  }
}

WeatherContainer.propTypes = {};

export default WeatherContainer;
