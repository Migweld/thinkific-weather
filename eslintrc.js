module.exports = {
  parser: "babel-eslint",
  env: {
    browser: true,
    es6: true,
    node: true,
    mocha: true
  },
  extends: ["eslint:recommended", "plugin:react/recommended"],
  rules: {
    "max-len": [
      1,
      120,
      2,
      {
        ignoreComments: true
      }
    ],
    "quote-props": [1, "consistent-as-needed"],
    "no-cond-assign": [2, "except-parens"],
    radix: 0,
    "space-infix-ops": 0,
    "no-unused-vars": [
      1,
      {
        vars: "local",
        args: "none"
      }
    ],
    "default-case": 0,
    "no-else-return": 0,
    "no-param-reassign": 0,
    quotes: 0,
    "import/no-mutable-exports": 0,
    "react/destructuring-assignment": 0,
    "require-jsdoc": 0,
    "comma-dangle": ["error", "never"],
    "newline-per-chained-call": [
      "error",
      {
        ignoreChainWithDepth: 2
      }
    ],
    indent: [
      "error",
      2,
      {
        MemberExpression: 2
      }
    ]
  }
};
