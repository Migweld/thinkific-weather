## Thinkific weather

A weather-app coding exercise for Thinkific. It uses a small NodeJS script to allow performant city searching (https://gitlab.com/Migweld/owm-cities).

A demo is available at https://thinkific-weather.firebaseapp.com/

### How to use

- Clone repo
- `cd <repo directory>`
- `yarn install`
- `yarn run start`
- Open browser to http://localhost:3000

### Testing (Jest)

- `yarn install`
- `yarn run test`

### Testing (Cypress)

- `yarn install`
- Ensure server is running with `yarn start`
- `yarn run cypress:open`
- Run all specs from cypress dialog

### Assumptions:

- Users mainly interested in main weather, min/max/current temperatures and humidity for a given 3-hour period
- Temperatures presented in metric

### Potential further improvements:

- Temperature conversion functionality with a switch to convert from/to Fahrenheit & Celcius
- Animations to indicate loading state
- Differing data sets based on type of user e.g. 'Pilot' view with barometric pressure, cloud base etc.
- Snow/Rainfall report
